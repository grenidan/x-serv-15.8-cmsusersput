from django.contrib import admin
from django.urls import include, path
from django.contrib.auth.views import LoginView as login


urlpatterns = [
    path('cms/', include('cms.urls')),
    path('calc/', include('calc.urls')),
    path('admin/', admin.site.urls),
    path('login', login.as_view()),
]